module.exports = function(app)
{
	//adding the validator module
	const { check, validationResult } = require('express-validator');
	const redirectLogin = (req, res, next) => {
		if (!req.session.userId ) {
			res.redirect('./login')
		} else { next (); }
	}

	app.get('/',function(req,res){
		res.render('index.html')
	});

	//route to display a new form
	app.get('/search',redirectLogin, function(req,res){
		res.render('search.html');
	});
	
	//route to dearch for a recipe in the database and retrive list of recipes based on search "keyword" 
	app.get('/search-result',redirectLogin, function(req,res) {
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';
		let keyword = req.query.keyword;
		MongoClient.connect(url, function (err, client) {
			if (err) throw err;
			var db = client.db('recipebank');
			//using the find function to retrive a list of recipes in the database
			db.collection('recipes').find({name: keyword}).toArray((findErr, results) => {
				if (findErr) throw findErr;
				else
					res.render('list.ejs', {availablerecipes:results});
				client.close();
			});
		});
	});
	
	//route to display a new form 
	app.get('/register', function (req,res) {
		res.render('register.html');                           
	});                                                  
	
	//route to collect and display form data from registration form
	app.post('/registered', [
			//checking if it is a valid email and password - password should be more than 5 characters and should have a number
			check('email','Your email is not valid').not().isEmpty().isEmail().normalizeEmail(), 
			check('password', 'Your password must be atleast 5 characters').not().isEmpty().isLength({min:5})
	], function (req,res) {
		const errors = validationResult(req);
		console.log(req.body);
			
		if(!errors.isEmpty())
		{
			//in case error, display the error in json format 
			return res.status(422).jsonp(errors.array());
			//res.redirect('./register');
		}
		else {
			// saving data in database
			var MongoClient = require('mongodb').MongoClient;
			var url = 'mongodb://localhost';
			//hashing password
			const bcrypt = require('bcrypt');
			const saltRounds = 10;
			const plainPassword = req.sanitize(req.body.password);

			bcrypt.hash(plainPassword, saltRounds, function(err, hashedPassword) {
				// Store hashed password in your database.

				MongoClient.connect(url, function(err, client) {
					if (err) throw err;
					var db = client.db ('recipebank');  
					
					//adding a new user to the users table
					db.collection('users').insertOne({
						first: req.body.first,
						last: req.body.last,
						email: req.body.email,
						username: req.body.username,
						//password: req.body.password
						//storing the hashed password
						hashedPassword: req.body.password
					});
					client.close();
				});
				res.send('Hello '+ req.body.first + ' '+ req.body.last +' you are now registered <br /> Your username is '+req.body.username+' <br >We will now send an email to your email address: '+ req.body.email +' to confirm that its correct!' + '<br />'+'<a href='+'./'+'>Home</a>');
			});
		}
	});
	
	//route to display a new form 
	app.get('/addrecipe',redirectLogin,  function (req,res) {
		res.render('addrecipe.html');
	});
	
	//route to collect and add a new recipe to the database
	app.post('/recipeadded', redirectLogin, function (req,res) {
		// saving data in database
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';                                                    
	
		MongoClient.connect(url, function(err, client) {
			if (err) throw err;
			var db = client.db ('recipebank');  
			//adding a new recipe to the recipes collection
			db.collection('recipes').insertOne({
				name: req.body.name,
				ingredients: req.body.ingredients,
				description: req.body.description,
				author: req.body.author
			});
			client.close();
			res.send(' This recipe has been added to the database,'+' <br />'+'Name: '+ req.body.name +'<br />'+ 'Author Name: ' +req.body.author+ '<br />'+ 'Ingredients: '+ req.body.ingredients+ '<br />'+'Description: '+ req.body.description + '<br />'+'<a href='+'./'+'>Home</a>');
		});
	});
	
	//route to update a form
	app.get('/updaterecipeform', redirectLogin, function (req,res) {
		var MongoClient = require('mongodb').MongoClient;
		var url ='mongodb://localhost';
		let currUser = req.session.userId;
		MongoClient.connect(url, function(err,client) {
			if(err) throw err;
			var db = client.db('recipebank');
			db.collection('recipes').find({author: currUser}).toArray((err, results) => {
				if(err) throw err;
				else{
					res.render('updaterecipeform.ejs', {userrecipes:results});
				}
				client.close();
			});
		});
	});

	//route to populate the update form
	app.get('/updaterecipe', redirectLogin, function (req,res) {
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';
		MongoClient.connect(url,function(err, client) {
			if(err) throw err;
			let rectoupdate = req.sanitize(req.query.name);
			var db = client.db('recipebank');
			var thequery = {name: rectoupdate, author: req.session.userId};
			db.collection('recipes').find(thequery).toArray((err, results) =>{
				if(err) throw err;
				else {
					console.log(results);
					res.render('updaterecipe.ejs',{userrecipes: results});
				}
			});
			client.close();
		});
	//	res.render('updaterecipe.html');
	});

	//route to collect and update a recipe in the database
	app.post('/recipeupdated', redirectLogin, [check('name').trim()], function (req,res) {
		// saving data in database
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';

		MongoClient.connect(url, function(err, client) {
			if (err) throw err;
			var db = client.db ('recipebank');
			//storing the old name of the recipe
			var recipedetails = {name: req.sanitize(req.body.name)};
			//storing the new updated version 
			var newvalues={  $set: {ingredients: req.body.ingredients, description: req.body.description}};
			//updating the collection with the new details, based on the name of the recipe
			db.collection('recipes').updateOne(recipedetails, newvalues, function(err, obj) {
				if(err) throw err;
				else
				{
					res.send(' This recipe has been updated '+'<br />'+'<a href='+'./'+'>Home</a>');
				}
				client.close();
			});
		});
	});

	//route to display a form
	app.get('/deleterecipe', redirectLogin, function (req,res) {
		var MongoClient = require('mongodb').MongoClient;
		var url ='mongodb://localhost';
		let currUser = req.session.userId;
		MongoClient.connect(url, function(err,client) {
			if(err) throw err;
			var db = client.db('recipebank');
			db.collection('recipes').find({author: currUser}).toArray((err, results) => {
				if(err) throw err;
				else{
					res.render('deleterecipe.ejs', {userrecipes:results});
				}
			});
			client.close();
		});
		//res.render('deleterecipe.html');
	});
 
	//route to collect and delete an entry from the collection 
	app.post('/recipedeleted', redirectLogin, function (req,res) {
		// saving data in database
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';

		MongoClient.connect(url, function(err, client) {
			if (err) throw err;
			var db = client.db ('recipebank');
			//storing the name of the recipe that has to be deleted
			let keyword = req.body.name;
			//deleting the corresponding entry from the collection
			db.collection('recipes').deleteOne({name: keyword}, function(err,obj) {
				if(err) throw err;
				res.send(' This recipe has been deleted '+'<br />'+'<a href='+'./'+'>Home</a>');
			});
			client.close();
		});
	});

	//route to display a list of available recipes in the database 
	app.get('/list', function(req, res) {
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';
		MongoClient.connect(url, function (err, client) {
			if (err) throw err;
			var db = client.db('recipebank');
			db.collection('recipes').find().toArray((findErr, results) => {
				if (findErr) throw findErr;
				else
					res.render('list.ejs', {userrecipes:results});
				client.close();
			});
		});
	});
	
	//route to display a form
	app.get('/login', function (req,res) {
		res.render('login.html');
	});

	//route to compare the collected form data with the data stored in the database and thendisplay a message if login is successful or not 
	app.post('/loggedin', function (req,res) {
		const saltRounds = 10;
		const plainPassword = req.body.password;
		const bcrypt  = require ('bcrypt');
		bcrypt.hash(plainPassword, saltRounds, function(err, hashedPassword) {
		// check form data hashed password with the password saved in DB
			if (err) throw err;
			var MongoClient = require('mongodb').MongoClient;
			var url = 'mongodb://localhost';
			MongoClient.connect(url, function(err, client) {
				if (err) throw err;
				var db = client.db ('recipebank');
				//using the find function to check for the coressponding name and password
				db.collection('users').findOne({username: req.body.username} ,function(err,result) {
					if (err) throw err;
					if(result == null){
						//if not found then display an appropriate message
						res.send('Login Unsuccessful, wrong username < br /> <a href='+'./'+'>Home</a>');
					}
					else {
						//checking if the password is correct or not 
						db.collection('users').findOne({ hashedPassword: plainPassword},function(err,result) {
							if(err) throw err;
							if(result == null){
								res.send('Login unsuccessfull, wrong password <br /> <a href='+'./'+'>Home</a>');
							}
							else {
								// **** save user session here, when login is successful
								req.session.userId = req.body.username;
								res.send('You have now successfully logged in!'+' <br />'+' <a href='+'./'+'>Home</a>');
							}
						})
					}
					client.close();
				})
			})
		});
	});
	
	//route to logout 
	app.get('/logout', redirectLogin, (req,res) => {
		req.session.destroy(err => {
			if (err) {
				return res.redirect('./')
			}
			res.send('You are now logged out.'+'<br />'+' <a href='+'./'+'>Home</a>');
		})
	});

	//route to create a simple API to display all the recipes in our database in json format
	app.get('/api', function (req,res) {
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost';
		MongoClient.connect(url, function (err, client) {
			if (err) throw err												
			var db = client.db('recipebank');                             
			db.collection('recipes').find().toArray((findErr, results) => { 
				if (findErr) throw findErr;
				else
					res.json(results);                                          
				client.close();                                                 
			});
		});
	});

}
